from notes.models import Note, User
from notes import db
from notes import retrieve_secrets
from notes import create_app
from dotenv import load_dotenv
import os
import pytest


@pytest.fixture(scope='module')
def test_client(app):
    return app.test_client()

@pytest.fixture(scope='module')
def app():
    load_dotenv()
    os.environ['TESTING'] = 'True'
    os.environ['SQLALCHEMY_TEST_DATABASE_URI'] = os.environ.get("SQLALCHEMY_DATABASE_TEST_URI")
    app = create_app()
    #create all the table
    with app.app_context():
        db.create_all()
    yield app
    # drop all tables
    with app.app_context():
        db.drop_all()
    os.environ.pop('TESTING')
    os.environ.pop('SQLALCHEMY_TEST_DATABASE_URI')

def test_create_app(app):
    assert app is not None
    assert app.config['SECRET_KEY'] is not None


def test_signup_login(app, test_client):
    with app.app_context():
        # send a POST request to the signup route with test data
        response = test_client.post('/sign-up', data={
            'email': 'testuser@test.com',
            'username': 'testuser',
            'password1': 'testpassword',
            'password2': 'testpassword'
        })
   
        assert response.status_code == 302
        assert User.query.count() == 1 
        print(response.data)


def test_add_note (app,test_client):
    with app.app_context():
        test_data = {'note':'This is a test note'}
        # send a POST request to the home route with test data
        response = test_client.post('/', data=test_data)
        note = Note.query.filter_by(data='This is a test note').first()

        assert note is not None
        assert response.status_code == 200
        assert Note.query.count() == 1 


def test_update_note(app, test_client):
    with app.app_context():
        note = Note.query.first()
    
        response = test_client.post(f'/update/{note.id}', follow_redirects=True)
        assert response.status_code == 200
        
        # simulate a POST request to the /update route
        update_data = {'note': 'Test note updated'}
        response = test_client.post('/update', data=update_data)
        assert response.status_code == 302

        
        # query the database to check if the note was updated
        note = Note.query.filter_by(data='Test note updated').first()
        assert note is not None


def test_delete_note(app,test_client):
    with app.app_context():
        note = Note.query.first()
        # delete the note
        response = test_client.post(f'/delete/{note.id}', follow_redirects=True)
        assert response.status_code == 200 
        # query the database to check if the note was deleted
        note = Note.query.filter_by(id=note.id).first()
        assert note is None
        
   


    

