from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
import os 
    
db = SQLAlchemy()

def retrieve_secrets(project_id, secret_ids):
    """
    Retrieve multiple secrets from the Secret Manager and return them as a dictionary.
    project_id: The ID of the GCP project where the secrets are stored.
    secret_ids: A list of secret IDs to retrieve.
    """
    from google.cloud import secretmanager
    client = secretmanager.SecretManagerServiceClient()
    secrets = {}
    for secret_id in secret_ids:
        name = f"projects/{project_id}/secrets/{secret_id}/versions/latest"
        try:
            response = client.access_secret_version(request={"name": name})
            payload = response.payload.data.decode("UTF-8")
            secrets[secret_id] = payload
        except Exception as e:
            print(f'Error occured while accessing secret {secret_id} : {e}')
    return secrets


def create_app():

     app = Flask(__name__)

     project_id = 'huddle-project-flask'
     secret_ids = ['SECRET_KEY','SQLALCHEMY_DATABASE_URI']
     secrets = retrieve_secrets(project_id, secret_ids)
     connection_name = os.environ.get("CONNECTION_NAME")

     app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY')
     
     if os.environ.get('TESTING'):
          app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQLALCHEMY_DATABASE_TEST_URI')
          app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY')
     else:
          app.config['SQLALCHEMY_DATABASE_URI']= secrets["SQLALCHEMY_DATABASE_URI"]
          app.config['SECRET_KEY'] = secrets["SECRET_KEY"]

     db.init_app(app)

     from notes.views import views
     from notes.auth import auth

     app.register_blueprint(views, url_prefix='/')
     app.register_blueprint(auth, url_prefix='/')

     from notes.models import User, Note

     with app.app_context():
          db.create_all()
     
     login_manager = LoginManager()
     login_manager.login_view = 'auth.login'
     login_manager.init_app(app)

     @login_manager.user_loader
     def load_user(id):
          # by default it will look at the primary key
          return User.query.get(int(id))

     return app





