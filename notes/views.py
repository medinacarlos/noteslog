from flask import Blueprint, render_template, request, flash, redirect, url_for, session, abort
from flask_login import login_required, current_user
from notes.models import Note
from notes import db
from pytz import timezone
from datetime import datetime

views = Blueprint('views',__name__)


@views.route('/', methods = ["GET","POST"])
@login_required
def home ():
    if request.method == "POST":
        note = request.form.get("note")
        if len(note) < 4:
            flash("The note must be longer than 3 characters",category="error")
        else:
            madrid_timezone = timezone('Europe/Madrid')
            madrid_time = datetime.now(madrid_timezone)
            new_note = Note(data=note, user_id = current_user.id,date=madrid_time)
            db.session.add(new_note)
            db.session.commit()

    return render_template("home.html", user= current_user)

@views.route("/delete/<int:note_id>", methods=["GET","POST"])
@login_required
def delete_post(note_id):
    note = Note.query.get(note_id)
    if note:
        if note.user_id == current_user.id:
            db.session.delete(note)
            db.session.commit()
            flash('Your post has been successfully deleted','success')
    return redirect(url_for('views.home'))

@views.route("/update/<int:note_id>", methods=["POST"])
@login_required
def get_note(note_id):
   session['note_id'] = note_id 
   note = Note.query.get(note_id)
   return render_template("edit.html", user=current_user, note=note)
    
@views.route("/update", methods=["POST","GET"])
@login_required
def update_note():
   note_id = session.get('note_id')
   note = Note.query.get(note_id)
   if request.method == 'POST' :
    if note.user_id == current_user.id:
        update_note = request.form.get('note')
        note.data = update_note
        madrid_timezone = timezone('Europe/Madrid')
        madrid_time = datetime.now(madrid_timezone)
        note.date = madrid_time
        db.session.commit()
        return redirect(url_for('views.home'))
    else:
        abort(403)
   else:
    return render_template ("home.html", user=current_user)
    
    
    
    
    
